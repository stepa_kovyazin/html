﻿#include<iostream>
using namespace std;
int main()
{
	int x,o,i=0;
	setlocale(LC_ALL, "rus");
	cout << "Добро пожаловать в игру КРЕСТИКИ НОЛИКИ:"<<endl;
	cout << "Инструкция:"<<endl;
	cout << "1. Игрок A ходит крестиками 'x' игрок B ходит '0' по очереди."<<endl;
	cout << "2. Побеждает тот, у кого выставленные им символы трижды пересекаются либо по горизонтали, либо по вертикали, либо по диагонали."<<endl;
	cout << "3. Если тройной комбинации нет, получается ничья!"<<endl;
	char array[9] = { '1','2','3','4','5','6','7','8','9' };
	cout <<"|"<< array[1-1] << "|" << array[2-1] << "|" << array[3-1] << "|" <<endl ;
	cout << "|" << array[4 - 1] << "|" << array[5 - 1] << "|" << array[6 - 1] << "|" << endl;
	cout << "|" << array[7 - 1] << "|" << array[8 - 1] << "|" << array[9 - 1] << "|" << endl;
	while ( i<9)
	{
		i = i + 1;
		cout << "Ход игрока А крестиками(Х),выберите клетку:";
		cin >> x;
		if (x < 1 or x>9)
		{
			while (x < 1 or x>9)
			{
				cout << "ERROR:некорректно введены данные"<<endl;
				cout << "укажите клетку ещё раз: ";
				cin >> x;
			}
		}
		if (array[x - 1] == '0')
		{
			while (array[x - 1] == '0')
			{	
				cout << "Данная клетка уже занята другим игроком,выберите другую: ";
				cin >> x;
			}
		}
		if (array[x - 1] == 'x')
		{
			while (array[x - 1] == 'x')
			{
				cout << "вы уже заняли данную клетку,выберите другую: ";
				cin >> x;
			}
		}
		array[x-1] = 'x';
		cout << "|" << array[1 - 1] << "|" << array[2 - 1] << "|" << array[3 - 1] << "|" << endl;
		cout << "|" << array[4 - 1] << "|" << array[5 - 1] << "|" << array[6 - 1] << "|" << endl;
		cout << "|" << array[7 - 1] << "|" << array[8 - 1] << "|" << array[9 - 1] << "|" << endl;
		if (array[0] =='x' and array[1] == 'x' and array[2] == 'x' or array[3] == 'x' and array[4] == 'x' and array[5] == 'x' or array[6] == 'x' and array[7] == 'x' and array[8] == 'x' or array[0] == 'x' and array[3] == 'x' and array[6] == 'x' or array[1] == 'x' and array[4] == 'x' and array[7] == 'x' or array[2] == 'x' and array[5] == 'x' and array[8] == 'x' or array[0]=='x' and array[4] == 'x' and array[8] == 'x' or array[2] == 'x' and array[4] == 'x' and array[6] == 'x')
		{
			cout << "Победил игрок А"<<endl;
			return 0;
		}
		cout << "Ход игрока B ноликами (0),выберите клетку:";
		i = i + 1;
		cin >> o  ;
		if (o < 1 or o>9)
		{
			while (o < 1 or o>9)
			{
				cout << "ERROR:некорректно введены данные" << endl;
				cout << "укажите клетку ещё раз: ";
				cin >> o;
			}
		}
		if (array[o - 1] == 'x')
		{
			while (array[o - 1] == 'x')
			{
				cout << "Данная клетка уже занята другим игроком,выберите другую: ";
				cin >> o;
			}
		}
		if (array[o - 1] == '0')
		{
			while (array[o - 1] == '0')
			{
				cout << "вы уже заняли данную клетку,выберите другую: ";
				cin >> o;
			}
		}
		array[o - 1] = '0';
		cout << "|" << array[1 - 1] << "|" << array[2 - 1] << "|" << array[3 - 1] << "|" << endl;
		cout << "|" << array[4 - 1] << "|" << array[5 - 1] << "|" << array[6 - 1] << "|" << endl;
		cout << "|" << array[7 - 1] << "|" << array[8 - 1] << "|" << array[9 - 1] << "|" << endl;
		if (array[0] == '0' and array[1] == '0' and array[2] == '0' or array[3] == '0' and array[4] == '0' and array[5] == '0' or array[6] == '0' and array[7] == '0' and array[8] == '0' or array[0] == '0' and array[3] == '0' and array[6] == '0' or array[1] == '0' and array[4] == '0' and array[7] == '0' or array[2] == '0' and array[5] == '0' and array[8] == '0' or array[0] == '0' and array[4] == '0' and array[8] == '0' or array[2] == '0' and array[4] == '0' and array[6] == '0')
		{
			cout << "Победил игрок В" << endl;
			return 0;

		}
	}
	system("pause");
	return 0;
}